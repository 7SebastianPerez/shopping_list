module.exports = function submitTitle(title) {
    var encoded = btoa(title);
    console.log("encoded: " + encoded);
    return fetch('/ingredients/' + encoded).then(res => res.json()).then(data => {
        if (!data){
            alert("Backend error...");
        } else if (Object.keys(data).indexOf("ingredients") > -1) {
            console.log(`${data.ingredients.length} new ingredients!`);
            return data.ingredients;
        } else {
            alert("Invalid search value. Try again.")
            return [];
        }
    });
};
