module.exports = function shareList(shoppingList, uuid) {
    console.log("Sharing list");
    return fetch(`/link/${uuid}`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify({"list": shoppingList})
    });
}