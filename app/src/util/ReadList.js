module.exports = function readList(path) {
    console.log("Reading list from db");
    return fetch(path).then(response => {
        if (!response.ok) {
            throw new Error("Error reading list HTTP " + response.status);
        }
        else {
            return response.json();
        }
    }).then(data => {
        if(data){
            return data[path.slice(6)]
        } else {
            return []
        }
    });
}