import React from 'react';
import { createMuiTheme } from '@material-ui/core/styles';
import { MuiThemeProvider } from '@material-ui/core';
import Root from './components/Root';

const theme = createMuiTheme({
    typography: {
        useNextVariants: true,
    },
});

function App() {
    return (
        <MuiThemeProvider theme={theme}>
            <Root />
        </MuiThemeProvider>
    );
}

export default App;
