import React, { useState } from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import { makeStyles } from '@material-ui/core/styles';

const submitRecipe = require('../../util/SubmitSearch');

const useStyles = makeStyles((theme) => ({
    search: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
        width: '80%'
    },
    text: {
        width: '540px',
    },
}));


export default function Search(props) {
    const [recipe, setRecipe] = useState("");
    const classes = useStyles();

    return (
        <div className={classes.search}>
            <TextField className={classes.text} variant="outlined" value={recipe} onChange={e=>setRecipe(e.target.value)}/>
            <Button variant="contained" size="large" color="secondary" component="span" onClick={()=>submitRecipe(recipe).then(props.addToShoppingList)}>
                Search
            </Button>
        </div>
    );
}