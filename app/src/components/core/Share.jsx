import React, {useState} from 'react';
import Popover from '@material-ui/core/Popover';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ShareIcon from '@material-ui/icons/Share';
import { v4 as uuidv4 } from 'uuid';

const shareList = require('../../util/ShareLink');


export default function Share(props){
    const [isOpen, setIsOpen] = useState(false);
    const [anchorEl, setAnchorEl] = useState(null);
    const localId = localStorage.getItem("SharingUuid");
    const uuid = localId ? localId : uuidv4();
    const handleOpen = (e) => {
        setAnchorEl(e.currentTarget); 
        setIsOpen(true); 
        shareList(props.shoppingList, uuid);
    };
    const handleClose = () => {
        setAnchorEl(null); 
        setIsOpen(false); 
    };
    if(!localId){
        localStorage.setItem("SharingUuid", uuid);
    }
    const popContent = `${window.location.hostname}${window.location.port === 80 ? "" : (':' + window.location.port)}/link/${uuid}`;

    return (
        <div>
            <Button
                variant="contained"
                color="default"
                startIcon={<ShareIcon />}
                onClick={handleOpen}
            >
                <b>Share</b>
            </Button>
            <Popover
                anchorEl={anchorEl}
                open={isOpen}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <Typography><a href={popContent}>{popContent}</a></Typography>
            </Popover>
      </div>
    );
}