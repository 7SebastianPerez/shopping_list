import React from 'react';

export default function ShoppingList(props) {
    return (
        <textarea
            rows="25"
            cols="50"
            value={props.shoppingList.join('\n')}
            onChange={e=> props.setShoppingList(e.target.value.split('\n'))}
        />
    );
}