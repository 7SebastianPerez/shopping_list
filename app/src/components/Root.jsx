import React, {useState, useEffect} from 'react';
import ShoppingList from './core/ShoppingList';
import Search from './core/Search';
import Share from './core/Share';
import {makeStyles} from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ShoppingCartIcon from '@material-ui/icons/ShoppingCart';
import {Icon, List} from '@material-ui/core';
const readList = require('../util/ReadList');

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
    },
    container: {
        backgroundColor: 'white',
        margin: 'auto',
        paddingTop: '15px',
    },
    icon: {
        marginRight: theme.spacing(2),
    },
    title: {
        flexGrow: 1,
    },
    app: {
        height: 'calc(100vh - 64px)',
        overflow: 'scroll'
    }
}));

function Root() {
    const [shoppingList, setShoppingList] = useState([]);
    const classes = useStyles();
    useEffect(() => {
        const path = window.location.pathname;
        if (path.indexOf('/link/') > -1){
            readList(path).then(data => setShoppingList(data));
        }
    }, []);

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <Icon className={classes.icon} color="inherit">
                        <ShoppingCartIcon/>
                    </Icon>
                    <Typography variant="h6" className={classes.title}>
                        Shopping List
                    </Typography>
                </Toolbar>
            </AppBar>
            <div className={classes.app}>
                <Container maxWidth="md" className={classes.container}>
                    <Typography variant="h4">
                        Get shopping!
                    </Typography>
                    <List>
                        <ListItem>
                            <ListItemText>This app makes it <b>easier</b> to aggregate ingredients for shopping.
                                Simply...</ListItemText>
                        </ListItem>
                        <ListItem>
                            <List>
                                <ListItem>
                                    <ListItemText>1. Add a recipe like <a
                                        href="https://www.foodnetwork.com/recipes/chicken-marsala">https://www.foodnetwork.com/recipes/chicken-marsala</a>.
                                    </ListItemText>
                                </ListItem>
                            </List>
                        </ListItem>
                        <ListItem>
                            <Search addToShoppingList={searchList => setShoppingList(shoppingList.concat(searchList))}/>
                        </ListItem>
                        <ListItem alignItems="flex-start">
                            <List>
                                <ListItem>
                                    <ListItemText>2. Edit your shopping list.</ListItemText>
                                </ListItem>
                                <ListItem>
                                    <ListItemText>3. Edit the list if you wish.</ListItemText>
                                </ListItem>
                                <ListItem>
                                    <ListItemText>4. Repeat until your list is complete. :)</ListItemText>
                                </ListItem>
                                <ListItem>
                                    <ListItemText>5. Save the list to take shopping.</ListItemText>
                                </ListItem>
                                <ListItem >
                                    <ListItemText>6. Share your list with your friends!</ListItemText>
                                </ListItem>
                                <ListItem>
                                   <Share shoppingList={shoppingList} />
                                </ListItem>
                            </List>
                            <List>
                                <ListItem>
                                    <ShoppingList shoppingList={shoppingList} setShoppingList={setShoppingList}/>
                                </ListItem>
                            </List>
                        </ListItem>

                    </List>
                </Container>
            </div>
        </div>
    )
}

export default Root;