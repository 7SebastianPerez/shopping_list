import os
from filelock import Timeout, FileLock
import pickle


DB = "db.pickle"
LOCK = "db.lock"

# initialize the lock
lock = FileLock(LOCK)

# initialize the database
with lock:
    with open(DB, 'wb') as handle:
        pickle.dump({}, handle, protocol=pickle.HIGHEST_PROTOCOL)

# write to db
def write(key, val):
    with lock.acquire(timeout=10):
        with open(DB, 'rb') as handle:
            db = pickle.load(handle)
        with open(DB, 'wb') as handle:
            db[key] = val
            pickle.dump(db, handle, protocol=pickle.HIGHEST_PROTOCOL)

# read from db
def read(key):
    with lock.acquire(timeout=10):
        with open(DB, 'rb') as handle:
            db = pickle.load(handle)
            if key in db:
                return db[key]

if __name__ == "__main__":
    print(read("hello"))
    write("hello", "world")
    print(read("hello"))
    print(read("world"))
    # lock.acquire()
    # open(DB, 'wb')
    # write("break", "please")