import requests
from bs4 import BeautifulSoup


BASE_SEARCH = "https://www.foodnetwork.com/search"
RECIPE_ONLY = "CUSTOM_FACET:RECIPE_FACET"
RECIPE_URL = "https://www.foodnetwork.com/recipes/"
def search(dish):
    log("dish", dish)
    rp = ""
    su = search_url(dish)
    rp = get_recipes(su)
    if rp is None:
        return None
    return rp


def search_url(dish):
    formatted = ""
    for w in dish.strip().split(' '):
        formatted += w
        formatted += '-'
    log("formatted", formatted)
    su = BASE_SEARCH + '/' + formatted + '/' + RECIPE_ONLY
    log("search url", su)
    return su


def get_recipes(search_url):
    response = requests.get(search_url)
    if response.status_code != 200:
        log("recipe search status", response.status_code)
        return None
    soup = BeautifulSoup(response.content, features='html.parser')
    if soup is None:
        log("couldn't parse", "")
        return None
    recipes = soup.findAll("div", {"class": "o-ResultCard__m-MediaBlock m-MediaBlock"})[0:5]
    if recipes is None:
        log("couldn't find div", recipes)
        return None
    recipe_urls = []
    for r in recipes:
        a = r.find('a')
        if a is None:
            log("couldn't a", a)
            return None
        url = a["href"]
        if url is None:
            return None
        log("recipe url", url)
        recipe_urls.append("https:"  + url)
    return recipe_urls

def get_ingredients(recipe_url):
    response = requests.get(recipe_url)
    if response.status_code != 200:
        log("ingredients status", response.status_code)
        return None
    soup = BeautifulSoup(response.content, features='html.parser')
    ingredients_p = soup.find_all("p", {"class": "o-Ingredients__a-Ingredient"})
    if len(ingredients_p) == 0:
        log("could not find ingredients", ingredients_p)
        return None
    ingredients = []
    for i in ingredients_p:
        ingredients.append(i.text.strip().strip(","))
    # log("ingredients", ingredients)
    return ingredients


def log(key, val):
    print(str(key) + ": " + str(val))


if __name__ == "__main__":
    print(search("lasagna"))
    print(search(" chicken marsala"))
    print(search(" Cinnamon Baked French Toast"))