from flask import Flask, request, abort
from food.engine import search, get_ingredients
from db.engine import write, read
import base64
import argparse

parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--debug', default=False, type=bool, help='debug or not')
app = Flask(__name__)


@app.route("/ingredients/<id>", methods=['GET'])
def ingredients(id):
    ing = get_ingredients(base64.b64decode(id).decode('utf-8'))
    if ing is None:
        abort(400)
    print("ingredients: " + str({id: ing}))
    return {"ingredients": ing, "id": id}, 200


@app.route("/choices/<dish>", methods=['GET'])
def choices(dish):
    dishes = search(dish)
    if dishes is None:
        abort(400)
    recipe_ingredients = {}
    for d in dishes:
        ings = get_ingredients(d)
        recipe_ingredients[d] = ings
    return {"dish": dish, "recipes":recipe_ingredients}, 200


@app.route("/link/<id>", methods=['GET', 'POST'])
def link(id):
    if request.method == "POST":
        if not request.json or not "list" in request.json:
            abort(400)
        try:
            write(id, request.json["list"])
        except:
            print("Error writing to DB")
            abort(500)
        return '', 201
    elif request.method == "GET":
        try:
            val = read(id)
        except:
            print("Error reading from DB")
            abort(500)
        if val is None:
            return '', 404
        return {id: val}, 200
    return '', 404

if __name__ == "__main__":
    args = parser.parse_args()
    print("debug: " + str(args.debug))
    if args.debug:
        app.run(debug=True)
    else:
        app.run(host="0.0.0.0")