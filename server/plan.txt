user->flow:
1. find recipes you want to shop for
2. go to shoppinglist.io
3. type in the names of recipes
4. have the list become populated

implementations:
- ingredients engine
    - take search and plug it into food network search
        - https://www.foodnetwork.com/recipes/search
        - https://www.foodnetwork.com/search/chicken-breast-
    - grab the first matching recipe
    - scrape the page for the ingredients
    - place ingrediants into the list of ingredients for the list
        - opportunity to add filtering in the future
- frontend
    1. simple get started instructions
    2. search bar (input validation)
    3. text area that is editable and gets populated with ingredients
        - they can add ingredients here
- shareable / persistent links
    - every new change to list causes it to be saved and generates a UUID
    - this UUID identifies the list
    - easy link share function


